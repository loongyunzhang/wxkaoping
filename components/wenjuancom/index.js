// components/testcom/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    receiveData: {
      type: null,
      observer: function (newVal, oldVal) {
         var that=this;
         that.setData({
           dataobj:newVal
         })
      }
    },
    statusData: {
      type: null,
      observer: function (newVal, oldVal) {
         var that=this;
         that.setData({
          status:newVal
         })
      }
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    dataobj:{},
    status:0,//1提交 0未提交
  },

  /**
   * 组件的方法列表
   */
  methods: {
    goDetail(){//点击跳转到详情
      var that=this;
      var dataobj=that.data.dataobj;
      wx.navigateTo({
        url: '/pages/wenjuan/index?id=',
      })
    },
  }
})
