// components/bgcom/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    receiveData: { // 属性名
      type: null, // 类型（必填），目前接受的类型包括：String, Number, Boolean, Object, Array, null（表示任意类型）
      observer:function(newVal,oldVal){
        var that = this;
        wx.getSystemInfo({
          success:function(res){
            that.setData({
              statusH:res.statusBarHeight
            })
          }
        })
        that.setData({
          pagetitle:newVal
        })
      }
    },
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    backOpt(){//返回
      wx.navigateBack({
        delta:1
      });
    },
  }
})
