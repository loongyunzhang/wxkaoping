// components/timucom/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    receiveData: { // 属性名
      type: null, // 类型（必填）
      observer: function (newVal, oldVal) {
        var that = this;
        that.setData({
          componentIndex: newVal
        })
      }
    },
    maxData: { // 属性名
      type: null, // 类型（必填）
      observer: function (newVal, oldVal) {
        var that = this;
        var maxNum = parseInt(newVal);
        var arrys = [];
        for (var i = 1; i <= maxNum; i++) {
          arrys.push(i);
        }
        that.setData({
          maxNum: maxNum,
          arrys: arrys,
        })
      }
    },
    userData: { // 属性名
      type: null, // 类型（必填）
      observer: function (newVal, oldVal) {
        var that = this;
        that.setData({
          username: newVal,
          chkNum:-1
        })
      }
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    componentIndex: -1,
    username: '',
    maxNum: 0,
    arrys: [],
    chkNum: -1,
  },

  /**
   * 组件的方法列表
   */
  methods: {
    chkFenOpt(e) { //点击选中
      var that=this;
      var chkNum= e.currentTarget.dataset.num;
      that.setData({
        chkNum: chkNum
      })
      that.triggerEvent('chknumobj',{
        chkNum:chkNum,
        componentIndex:that.data.componentIndex,
      });
    },
  }
})