// pages/wenjuan/index.js
var requestUrl = getApp().globalData.requestUrl;
var WxRequest = require('../../utils/WxRequest.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    currIndex: 1,
    summary: 5,
    list: [{
      username: '安壮',
      maxNum: 10,
      chkNum: 0,
    }, {
      username: '祝菲邦',
      maxNum: 8,
      chkNum: 0,
    }, {
      username: '耿莎',
      maxNum: 6,
      chkNum: 0,
    }, {
      username: '耿莎A',
      maxNum: 5,
      chkNum: 0,
    }, {
      username: '安壮',
      maxNum: 4,
      chkNum: 0,
    }, ],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },
  getchknumobj(e) { //获取选中的值
    var that = this;
    var obj = e.detail;
    var list = that.data.list;
    list[obj.componentIndex].chkNum = obj.chkNum;
    that.setData({
      list: list
    })
  },
  preOpt() { //上一题
    var that = this;
    var currIndex = that.data.currIndex;
    that.setData({
      currIndex: currIndex - 1
    })
  },
  nextOpt() { //下一题
    var that = this;
    //验证必填
    var list = that.data.list;
    var IsAllChk = false,
      userName = '';
    for (var i = 0; i < list.length; i++) {
      if (list[i].chkNum == 0 || list[i].chkNum == undefined || list[i].chkNum == null) {
        IsAllChk = true;
        userName = list[i].username;
        break;
      }
    }
    if (IsAllChk) {
      WxRequest.ShowAlert("请给" + userName + "打分");
    } else {
      var currIndex = that.data.currIndex;
      for (var i = 0; i < list.length; i++) {
        list[i].chkNum=0;
      }
      that.setData({
        currIndex: currIndex + 1,
        list:list,
      })
    }
  },
  PostOpt() { //提交操作
    var that = this;
    wx.showModal({
      cancelColor: '#666666',
      cancelText: '取消',
      confirmColor: '#000000',
      confirmText: '确定',
      content: '提交后不可再次修改，是否提交？',
      showCancel: true,
      title: '',
      success: (result) => {}
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})