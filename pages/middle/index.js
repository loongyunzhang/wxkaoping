// pages/middle/index.js
var requestUrl = getApp().globalData.requestUrl;
var WxRequest = require('../../utils/WxRequest.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.reLaunch({
      url: '../home/index',
    })
    // getApp().ChargeLogin().then(res=>{
    //   if(getApp().globalData.WxUserId == 0){
    //     wx.navigateTo({
    //       url: '../../wxauth/pages/wxlogin/index',
    //     })
    //   }else{
    //     wx.getStorage({
    //       key:"loginObj",
    //       success:function(res){
    //         wx.reLaunch({
    //           url: '../home/index',
    //         })
    //       },
    //       fail:function(){
    //         wx.reLaunch({
    //           url: '../../wxauth/pages/wxlogin/index',
    //         })
    //       }
    //     })
    //   }
    // })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})