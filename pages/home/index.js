// pages/home/index.js
var requestUrl = getApp().globalData.requestUrl;
var WxRequest = require('../../utils/WxRequest.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    showTestModal:false,//扫码显示测试浮窗
    testobj:{
      Title:"2020年现代设计史",
      Score:100,
      Duration:60,
    },
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },
  goInfoOpt() { //个人资料
    wx.navigateTo({
      url: '../auth/info',
    })
  },
  tapMenu(e) { //答题和抢答 1:答题  2：抢答
    var that=this;
    var type = e.currentTarget.dataset.type;
    // wx.scanCode({
    //   scanType: ['qrCode'],
    //   success: function (res) {
    //   }
    // });

    if(type==1){//考试
      that.setData({
        showTestModal:true
      })
    }else{//抢答
      wx.navigateTo({
        url: '../qiangda/index',
      })
    }
  },
  closeTip(){//关闭浮窗
    this.setData({
      showTestModal:false
    })
  },
  goTestList() { //我的考试列表
    wx.navigateTo({
      url: '../test/list',
    })
  },
  goQiangDaList() { //我的抢答列表
    wx.navigateTo({
      url: '../qiangda/list',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})