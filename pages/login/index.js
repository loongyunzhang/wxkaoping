// wxauth/pages/login/index.js
var requestUrl = getApp().globalData.requestUrl;
var WxRequest = require('../../utils/WxRequest.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    account: '',
    pwd: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },
  getAccount(e) { //获取账号
    this.setData({
      account: e.detail.value
    })
  },
  getPwd(e) { //获取密码
    this.setData({
      pwd: e.detail.value
    })
  },
  loginOpt() { //登录操作
    var that = this;
    var account = that.data.account,
      pwd = that.data.pwd;
    if (account == "") {
      WxRequest.ShowAlert("请输入账号");
    } else if (pwd == "") {
      WxRequest.ShowAlert("请输入密码");
    } else {
      //TODO 请求接口
      wx.setStorage({
        key: 'loginObj',
        data:'',
        success: function () {
          wx.navigateBack({
            delta: 1,
            fail: function () {
              wx.reLaunch({
                url: '/pages/middle/index',
              })
            }
          });
        }
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})