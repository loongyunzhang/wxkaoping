// wxauth/pages/auth/study.js
var WxRequest = require('../../utils/WxRequest.js');
var validator = require('../../utils/validator.js');
var requestUrl = getApp().globalData.requestUrl;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    tx: "", //头像
    nickName: "", //昵称
    phone: "", //手机号
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  chooseTx(e) { //上传头像
    var that = this;
    console.error("chooseTx", e);
    that.setData({
      tx: e.detail.avatarUrl
    })
  },
  getNickName(e) { //获取昵称
    this.setData({
      nickName: e.detail.value
    })
  },
  getPhoneNumber(e) { //获取手机号
  
    this.setData({
      phone: e.detail.value
    })
  },
  finishOpt() { //点击完成
    var that = this;
    var tx = that.data.tx, //头像
      nickName = that.data.nickName, //昵称
      phone = that.data.phone; //姓名

    if (tx == "") {
      WxRequest.ShowAlert("请上传头像");
    } else if (nickName == "") {
      WxRequest.ShowAlert("请输入昵称");
    } else if (phone == "") {
      WxRequest.ShowAlert("请输入手机号");
    } else if (!validator.validateMobile(phone)) {
      WxRequest.ShowAlert("手机号不正确");
    } else {
      //TODO 提交表单
      var url = requestUrl + "/API/LoginApi/CompleteUserInfo?verifyCode=" + code;
      var params = {
        ID: getApp().globalData.WxUserId,
        Avatar: that.data.tx,
        NickName: that.data.nickName,
        ReadName: that.data.name,
        Sex: sexs[sex].Key,
        Asign: that.data.desc,
        mobile: that.data.phone
      };
      WxRequest.PostRequest(url, params).then(res => {
        if (res.data.success) {
          getApp().globalData.userInfo = {
            Avatar: that.data.tx, //头像
            NickName: that.data.nickName, //昵称
            ReadName: that.data.name, //姓名
            Sex: sexs[sex].Key, //性别
            Asign: that.data.desc, //签名
            mobile: that.data.phone
          };
          wx.navigateBack({
            delta: 1,
          })
        } else {
          WxRequest.ShowAlert(res.data.msg);
        }
      })
    }
  },
  logout(){//退出操作
    wx.navigateTo({
      url: '../login/index',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})